﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.tree
{
    public class BinaryTree<T> 
    {
        public BinaryTree<T> left { get; set; }
        public BinaryTree<T> right { get; set; }
        public T value { get; set; }

        public static List<T> Deep { get; set; }
        static BinaryTree()
        {
            Deep = new List<T>();
        }

        public BinaryTree(T val)
        {
            this.value = val;
        }

        public void AddLefthNode(T val)
        {
            if (left == null)
            {
                left = new BinaryTree<T>(val);
            }
            else
            {
                var newNode = new BinaryTree<T>(val);
                newNode.left = left;
                left = newNode;
            }
        }
        public void AddRightNode(T val)
        {
            if (right == null)
            {
                right = new BinaryTree<T>(val);
            }
            else
            {
                var newNode = new BinaryTree<T>(val);
                newNode.right = right;
                right = newNode;
            }
        }

        /// <summary>
        /// Поиск в глубину предварительный обход
        /// </summary>
        /// <param name="node"></param>
        public void DFSPreOrder(BinaryTree<T> node)
        {
            Deep.Add(node.value);
            if (node.left != null)
            {
                DFSPreOrder(node.left);
            }

            if (node.right != null)
            {
                DFSPreOrder(node.right);
            }
            return;
        }

        /// <summary>
        /// Поиск в глубину симметричный
        /// </summary>
        public void DFSInOrder(BinaryTree<T> node)
        {
            if (node.left != null)
            {
                DFSInOrder(node.left);
            }
            Deep.Add(node.value);
            if (node.right != null)
            {
                DFSInOrder(node.right);
            }
        }

        /// <summary>
        /// Поиск в глубину обратный
        /// </summary>
        /// <param name="node"></param>
        public void DFSPostOrder(BinaryTree<T> node)
        {
            if (node.left != null)
            {
                DFSPostOrder(node.left);
            }
            if (node.right != null)
            {
                DFSPostOrder(node.right);
            }
            Deep.Add(node.value);
        }

        public void BFS(BinaryTree<T> node)
        {
            var queue = new Queue<BinaryTree<T>>();
            queue.Enqueue(node);
            Deep.Add(node.value);
            while(queue.Count != 0)
            {
                var node_ = queue.Dequeue();
                
                if (node_.left != null)
                {
                    Deep.Add(node_.left.value);
                    queue.Enqueue(node_.left);
                }
                
                if (node_.right != null)
                {
                    Deep.Add(node_.right.value);
                    queue.Enqueue(node_.right);
                }
                
            }
        }




        //foreach (var itr in queue)
        //{
        //    if (itr.value != null)
        //    {
        //        Deep.Add(itr.value);
        //    }
        //}
        public override string ToString()
        {
            return value.ToString();
        }
    }
}
