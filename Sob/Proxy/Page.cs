﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Proxy
{
    class Page
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Text { get; set; }
    }
}
