﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task.FizzBuzz
{
    public class FizzBuzz
    {
        public FizzBuzz()
        {
            for (int i = 0; i < 100; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    Console.WriteLine("fizz buzz");
                }
                if (i % 3 == 0)
                {
                    Console.WriteLine("fizz");
                }
                if(i % 5 == 0)
                {
                    Console.WriteLine("buzz");
                }
            }
        }
    }
}
