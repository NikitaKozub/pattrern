﻿select  Departments.Name, avg(Employees.Salary) as avg_salary
from Departments inner join Employees on Departments.Id = Employees.DepartmentId 
group by Departments.Name
 order by avg_salary ASC
 --offset 2 rows
 --FETCH FIRST  1 rows only