﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob
{
    public class Car : Transport
    {
        protected override int MaxSpeed
        {
            get;set;
        }

        public Car(int maxSpeed) : base(maxSpeed)
        {
            MaxSpeed = maxSpeed;
        }
    }

    public class Bike: Transport
    {
        protected override int MaxSpeed
        {
            get; set;
        }
        public Bike(int maxSpeed) : base(maxSpeed)
        {
            MaxSpeed = maxSpeed;
        }
    }
}
