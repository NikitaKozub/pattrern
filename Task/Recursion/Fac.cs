﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task.Recursion
{
    public class Fac
    {
        public int FacCalculation(int x)
        {
            if (x == 1)
            {
                return 1;
            }
            else
            {
                return x * FacCalculation(x - 1);
            }
        }
    }
}
