﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Decorator
{
    class ItalianPizza : Pizza
    {
        public ItalianPizza() : base("Итальянская пицца")
        { }
        public override int GetCost()
        {
            return 10;
        }
    }
}
