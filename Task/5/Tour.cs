﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Task._5
{
    public class Tour
    {
        public static double CalculationDist(double catet, double gipotenuza)
        {
            double catet_ = Math.Pow(catet, 2);
            double gipotenuza_ = Math.Pow(gipotenuza, 2);
            double result = Math.Sqrt(gipotenuza_ - catet_);
            return result;
        }

        static int CountFriendForWalk;
        private static void CalculateCountFriendForWalk(string[] arrFriends, string[][] ftwns)
        {
            CountFriendForWalk = 0;
            for (int i = 0; i < arrFriends.Length; i++)
            {
                for (int j = 0; j < ftwns.Length; j++)
                {
                    if (ftwns[j][0] == arrFriends[i])
                    {
                        CountFriendForWalk++;
                    }
                }
            }
        }
       
        public static int tour(string[] arrFriends, string[][] ftwns, Hashtable h)
        {
            double dist = 0;
            string prevFrend = ftwns[0][1];
            CalculateCountFriendForWalk(arrFriends, ftwns);
            for (int i = 0; i < arrFriends.Length; i++)
            {
                for (int j = 0; j < ftwns.Length; j++)
                {
                    if ((i == 0 || j == CountFriendForWalk || i == CountFriendForWalk - 1 /*i == arrFriends.Length-1 || i == ftwns.Length - 1 || j == ftwns.Length - 1*/) 
                        && ftwns[j][0] == arrFriends[i])
                    {
                        dist += (double)h[ftwns[j][1]];
                        if (i == 0)
                        {
                            break;
                        }
                    }

                    if (ftwns[j][0] == arrFriends[i])
                    {
                        dist += CalculationDist((double)h[prevFrend], (double)h[ftwns[j][1]]);
                        prevFrend = ftwns[j][1];
                    }
                }
            }
            // your code
            return (int)dist;
        }
    }
}
