﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Prototype
{
    interface IFigure
    {
        IFigure Clone();
        void GetInfo();
    }
}
