﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Factory
{
    public abstract class Product
    { }

    public class ConcreteProductA : Product
    { }

    public class ConcreteProductB : Product
    { }

    public abstract class Creator
    {
        public abstract Product FactoryMethod();
    }

    public class ConcreteCreatorA : Creator
    {
        public override Product FactoryMethod() { return new ConcreteProductA(); }
    }

    public class ConcreteCreatorB : Creator
    {
        public override Product FactoryMethod() { return new ConcreteProductB(); }
    }
}
