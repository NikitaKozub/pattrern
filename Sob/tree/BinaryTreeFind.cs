﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Sob.tree
{
    public class BinaryTreeFind<T> : IComparer<T> where T : IComparable<T>
    {
        public BinaryTreeFind<T> left { get; set; }
        public BinaryTreeFind<T> right { get; set; }
        public T value { get; set; }

        public static List<T> Deep { get; set; }
        static BinaryTreeFind()
        {
            Deep = new List<T>();
        }


        public BinaryTreeFind(T val)
        {
            this.value = val;
        }

        public void AddNewNode(BinaryTreeFind<T> node, T value)
        {
            if (Compare(node.value, value) < 0)
            {
                if (node.right == null)
                {
                    node.right =  new BinaryTreeFind<T>(value);
                    return;
                }
                else
                {
                    AddNewNode(node.right, value);
                }
            }
            if (Compare(node.value, value) > 0)
            {
                if (node.left == null)
                {
                    node.left = new BinaryTreeFind<T>(value);
                    return;
                }
                else
                {
                    AddNewNode(node.left, value);
                }
            }
            if (Compare(node.value, value) == 0)
            {
                
            }
            return;
        }

        public BinaryTreeFind<T> FindNode(BinaryTreeFind<T> node, T findValue)
        {
            if (Compare(node.value, findValue) < 0 && node.right != null)
            {
                return FindNode(node.right, findValue);
            }
            if (Compare(node.value, findValue) > 0 && node.left != null)
            {
                return FindNode(node.left, findValue);
            }

            if (Compare(node.value, findValue) == 0)
            {
                return node;
            }
            return null;
        }

        /// <summary>
        /// Поиск в глубину предварительный обход
        /// </summary>
        /// <param name="node"></param>
        public void DFSPreOrder(BinaryTreeFind<T> node)
        {
            Deep.Add(node.value);
            if (node.left != null)
            {
                DFSPreOrder(node.left);
            }

            if (node.right != null)
            {
                DFSPreOrder(node.right);
            }
            return;
        }
        public void DFSPreOrder(BinaryTree<T> node)
        {
            Deep.Add(node.value);
            if (node.left != null)
            {
                DFSPreOrder(node.left);
            }

            if (node.right != null)
            {
                DFSPreOrder(node.right);
            }
            return;
        }

        public bool DeleteNode(BinaryTreeFind<T> prevNode, BinaryTreeFind<T> currentNode, T findValue)
        {
            if (Compare(currentNode.value, findValue) < 0 && currentNode.right != null)
            {
                return DeleteNode(currentNode, currentNode.right, findValue);
            }
            if (Compare(currentNode.value, findValue) > 0 && currentNode.left != null)
            {
                return DeleteNode(currentNode, currentNode.left, findValue);
            }

            if (Compare(currentNode.value, findValue) == 0)
            {
                // Если у удаляемого узла нет потомков
                if (currentNode.left == null && currentNode.right == null)
                {
                    if(prevNode.right == currentNode)
                    {
                        prevNode.right = null;
                    }
                    if (prevNode.left == currentNode)
                    {
                        prevNode.left = null;
                    }
                }
                // Если у удаляемого узла, только правый или левый потомок
                if ((currentNode.left == null && currentNode.right != null) ||
                    (currentNode.left != null && currentNode.right == null))
                {
                    if (prevNode.right == currentNode)
                    {
                        if (currentNode.right != null)
                        {
                            prevNode.right = currentNode.right;
                        }
                        if (currentNode.left != null)
                        {
                            prevNode.right = currentNode.left;
                        }
                    }
                    if (prevNode.left == currentNode)
                    {
                        if (currentNode.right != null)
                        {
                            prevNode.left = currentNode.right;
                        }
                        if (currentNode.left != null)
                        {
                            prevNode.left = currentNode.left;
                        }
                    }
                }
                // Если у уадляемого узла и левый и правый потомок
                if (currentNode.left != null && currentNode.right != null)
                {
                    if (currentNode.right.left != null)
                    {
                        if (prevNode.right == currentNode)
                        {
                            var minNode = FindMinNode(currentNode, currentNode.left.value);
                            prevNode.right = minNode;
                            minNode.right = currentNode.right;
                            minNode.left = currentNode.left;
                        }
                        if (prevNode.left == currentNode)
                        {
                            var minNode = FindMinNode(currentNode, currentNode.left.value);
                            prevNode.left = minNode;
                            minNode.right = currentNode.right;
                            minNode.left = currentNode.left;
                        }
                    }
                    if (currentNode.right.left == null)
                    {
                        if (prevNode.right == currentNode)
                        {
                            prevNode.right = currentNode.right;
                            currentNode.right.left = currentNode.left;
                        }
                        if (prevNode.left == currentNode)
                        {
                            prevNode.left = currentNode.right;
                            currentNode.right.left = currentNode.left;
                        }
                    }
                }
            }

            return true;
        }

        public BinaryTreeFind<T> FindMinNode(BinaryTreeFind<T> node, T findValue)
        {
            if (Compare(node.value, findValue) > 0 && node.left != null)
            {
                return FindNode(node.left, findValue);
            }

            if (node.left == null)
            {
                return node;
            }
            return node;
        }


        public int Compare(T x, T y)
        {
            if (x.CompareTo(y) > 0)
            {
                return 1;
            }
            if (x.CompareTo(y) < 0)
            {
                return -1;
            }
            return 0;
        }
    }
}
