﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task.Morze._4;
using NUnit.Framework;
using System;
using System.Text;
using Task._6;
using System.Collections;
using Task._5;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("hey", HammingCode.decode("100111111000111001000010000111111000000111001111000111110110111000010111"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("The Sensei told me that i can do this kata", HammingCode.decode("000111000111000111000100000111111000111000001000000111111000000111000111000100111000000000000000000111000111000000111111000111111000000111000111000111111000111111111000000111111111000000111111000110111000010111000111000111111000111001000111000000111000000000000000000111111111000111000000000111111000111111111111000111111000111111000000000111111000000111000001000000111000000000001000000111111000111111000111000111111000000111000111000000111000000000000000000111111111000111000000000111111000111000000000000111111000000010000111000111111111000111000000000100111000000000000000000111111000111000000111000000111000000000000000000111111000000000111111000111111000000000000111000111111000111111111000000000111000000000000010000111111000000111000000000111111000111111110111000000111000000000000000000111111111000111000000000111111000111000000000000111111000111000000111000111111111000000111111000000111000000000000000000111111000111000111111000111111000000000000111000111111111000111000000000111111000000000000111"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("T3st", HammingCode.decode("000111000111000111000001000000111111000000111111000111111111000000111011000111111111000111000000"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("T?st!%", HammingCode.decode("000111000111000111000010000000111111111111011111000111111111000000111111000111101111000111000000000000111000000000000111000000111000000111000111"));
        }

        [TestMethod]
        public void TestMethod2()
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("000111111000111000000000000111111000000111000111000111111111111000000111", HammingCode.encode("hey"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("000111000111000111000000000111111000111000000000000111111000000111000111000000111000000000000000000111000111000000111111000111111000000111000111000111111000111111111000000111111111000000111111000111111000000111000111000111111000111000000111000000111000000000000000000111111111000111000000000111111000111111111111000111111000111111000000000111111000000111000000000000111000000000000000000111111000111111000111000111111000000111000111000000111000000000000000000111111111000111000000000111111000111000000000000111111000000000000111000111111111000111000000000000111000000000000000000111111000111000000111000000111000000000000000000111111000000000111111000111111000000000000111000111111000111111111000000000111000000000000000000111111000000111000000000111111000111111111111000000111000000000000000000111111111000111000000000111111000111000000000000111111000111000000111000111111111000000111111000000111000000000000000000111111000111000111111000111111000000000000111000111111111000111000000000111111000000000000111", HammingCode.encode("The Sensei told me that i can do this kata"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("000111000111000111000000000000111111000000111111000111111111000000111111000111111111000111000000", HammingCode.encode("T3st"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("000111000111000111000000000000111111111111111111000111111111000000111111000111111111000111000000000000111000000000000111000000111000000111000111", HammingCode.encode("T?st!%"));
        }

        [TestMethod]
        public void TestExampleFromDescription()
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(".", MorseCodeDecoder.DecodeBits("1"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("..", MorseCodeDecoder.DecodeBits("101"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(". .", MorseCodeDecoder.DecodeBits("10001"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(".-", MorseCodeDecoder.DecodeBits("10111"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("--", MorseCodeDecoder.DecodeBits("1110111"));

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(".... . -.--   .--- ..- -.. .", MorseCodeDecoder.DecodeBits("1100110011001100000011000000111111001100111111001111110000000000000011001111110011111100111111000000110011001111110000001111110011001100000011"));

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(".", MorseCodeDecoder.DecodeBits("000000011100000"));

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("--", MorseCodeDecoder.DecodeBits("11111100111111"));

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(". .", MorseCodeDecoder.DecodeBits("111000000000111"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("- .... .   --.- ..- .. -.-. -.-   -... .-. --- .-- -.   ..-. --- -..-   .--- ..- -- .--. ...   --- ...- . .-.   - .... .   .-.. .- --.. -.--   -.. --- --. .-.-.-", MorseCodeDecoder.DecodeBits("00011100010101010001000000011101110101110001010111000101000111010111010001110101110000000111010101000101110100011101110111000101110111000111010000000101011101000111011101110001110101011100000001011101110111000101011100011101110001011101110100010101000000011101110111000101010111000100010111010000000111000101010100010000000101110101000101110001110111010100011101011101110000000111010100011101110111000111011101000101110101110101110"));
        }

        private static object[] Basic_Test_Cases1 = new object[]
        {
            new object[]
            {
                new object[] {}
            }
        };

        private static object[] Basic_Test_Cases2 = new object[]
        {
            new object[]
                {
                    new object[] {1, 2, 3}
                }
        };

        private static object[] Basic_Test_Cases3 = new object[]
        {
            new object[]
              {
                new object[] {"x", "y", new object[] {"z"}}
              }
        };

        private static object[] Basic_Test_Cases4 = new object[]
        {
            new object[]
            {
                new object[] { new object[] { new object[] { new object[] { new object[] { new object[] { new object[] { new object[] { new object[] {}
                    }}}}}}}}
            }
        };
        private static object[] Basic_Test_Cases5 = new object[]
        {
            new object[]
            {
                new object[] {1, 2, new object[] {3, 4, new object[] {5} }
                }
            }
        };
        private static object[] Basic_Test_Cases6 = new object[]
        {
            new object[]
            {
                new object[] {new object[] { new object[] { 5, 6 }, 6}, new object[] {5, 6}, new object[] {5, 6}, new object[] {3, 4, new object[] {5, 6} }
                }
            }
        };


        [TestMethod]
        public void TestExampleDeep()
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(0, Kata.DeepCount(Basic_Test_Cases1));
            Kata.count = 0;
            Kata.fuckTest = 1;
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(3, Kata.DeepCount(Basic_Test_Cases2));
            Kata.count = 0;
            Kata.fuckTest = 1;
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(4, Kata.DeepCount(Basic_Test_Cases3));
            Kata.count = 0;
            Kata.fuckTest = 1;
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(8, Kata.DeepCount(Basic_Test_Cases4));
            Kata.count = 0;
            Kata.fuckTest = 1;
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(7, Kata.DeepCount(Basic_Test_Cases5));
            Kata.count = 0;
            Kata.fuckTest = 1;
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(17, Kata.DeepCount(Basic_Test_Cases6));
        }

        [TestMethod]
        public void Test6()
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("Thi11s is21 T24est 25a", You_order__please.Order("is21 Thi11s T24est 25a"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("Fo1r the2 g3ood 4of th5e pe6ople", You_order__please.Order("4of Fo1r pe6ople g3ood th5e the2"));
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual("", You_order__please.Order(""));
        }

        string[] friends1;
        Hashtable distTable1;
        [TestMethod]
        public void Test6Tour1()
        {
            friends1 = new string[] { "A1", "A2", "A3", "A4", "A5" };
            string[][] fTowns1 = { new string[] { "A1", "X1" }, new string[] { "A2", "X2" }, new string[] { "A3", "X3" }, new string[] { "A4", "X4" } };
            Hashtable distTable1 = new Hashtable { { "X1", 100.0 }, { "X2", 200.0 }, { "X3", 250.0 }, { "X4", 300.0 } };
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(889, Tour.tour(friends1, fTowns1, distTable1));
        }
        [TestMethod]
        public void Test6Tour2()
        {
            friends1 = new string[] { "A1", "A2" };
            string[][] fTowns2 = { new string[] { "A1", "X1" }, new string[] { "A2", "X2" }, new string[] { "A3", "X3" }, new string[] { "A4", "X4" }, new string[] { "A5", "X5" } };
            distTable1 = new Hashtable { { "X1", 50.0 }, { "X2", 70.0 }, { "X3", 90.0 }, { "X4", 110.0 }, { "X5", 150.0 } };
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(168, Tour.tour(friends1, fTowns2, distTable1));
        }

        [TestMethod]
        public void Test6Tour3()
        {
            friends1 = new string[] { "B1", "B2", "B4", "B5", "B6" };
            string[][] fTowns2 = { new string[] { "B1", "Y1" }, new string[] { "B2", "Y2" }, new string[] { "B3", "Y3" }, new string[] { "B4", "Y4" }, new string[] { "B5", "Y5" } };
            distTable1 = new Hashtable { { "Y1", 60.0 }, { "Y2", 80.0 }, { "Y3", 100.0 }, { "Y4", 110.0 }, { "Y5", 150.0 } };
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(440, Tour.tour(friends1, fTowns2, distTable1));
        }
    }
}