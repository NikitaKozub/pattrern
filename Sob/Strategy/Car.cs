﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Strategy
{
    class Car
    {
        protected int passengers; // кол-во пассажиров
        protected string model; // модель автомобиля
        public IMovable Movable { private get; set; }

        public Car(int num, string model, IMovable mov)
        {
            this.passengers = num;
            this.model = model;
            Movable = mov;
        }
        
        public void Move()
        {
            Movable.Move();
        }
    }
}
