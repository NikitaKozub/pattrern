﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.VirtualObberideNew
{
    public class A
    {
        public virtual void aa()
        {
            Console.WriteLine("A");
        }
        public void bb()
        {
            Console.WriteLine("AA");
        }
    }

    public class B : A
    {
        public override void aa()
        {
            Console.WriteLine("B");
        }
        public void bb()
        {
            Console.WriteLine("BB");
        }
    }

    public class C : B
    {
        public override void aa()
        {
            Console.WriteLine("C");
        }

        public void bb()
        {
            Console.WriteLine("CC");
        }
    }

    public class E : B
    {
        public override void aa()
        {
            Console.WriteLine("E");
        }

        public void bb()
        {
            Console.WriteLine("EE");
        }

        public static long IpsBetween(string start, string end)
        {
            int result = 0;
            string[] arrayStart = start.Split('.');
            int[] arrayStartInt = Array.ConvertAll(arrayStart, s => int.Parse(s));
            string[] arrayEnd = end.Split('.');
            int[] arrayEndInt = Array.ConvertAll(arrayEnd, s => int.Parse(s));
            int xPow;
            int j = -1;
            for (int i = arrayEndInt.Length-1; i > 0; i--)
            {
                j++;
                if (arrayEndInt[i] != arrayStartInt[i])
                {
                    xPow = (int)Math.Pow(256, j);
                    result += (arrayEndInt[i] - arrayStartInt[i]) * xPow;
                }
            }
            return result;
        }
    }
}
