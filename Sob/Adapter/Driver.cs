﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Adapter
{
    class Driver
    {
        public void Travel(ITransport transport)
        {
            transport.Drive();
        }
    }
}
