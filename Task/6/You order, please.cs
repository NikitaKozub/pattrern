﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task._6
{
    public class You_order__please
    {
        private static string[] result;
        private static StringBuilder tmp = new StringBuilder();

        public static void Swap(int id1, int id2)
        {
            tmp.Append(result[id1]);
            result[id1] = result[id2];
            result[id2] = tmp.ToString();
            tmp.Clear();
        }

        public static string[] OrderBy(string[] words)
        {
            result = words;
            string word1;
            string word2;
            int value1;
            int value2;
            for (int i = 0; i < words.Length; i++)
            {
                for (int j = 0; j < words.Length - 1; j++)
                {
                    word1 = words[j];
                    word2 = words[j + 1];
                    int.TryParse(string.Join("", word1.Where(c => char.IsDigit(c))), out value1);
                    int.TryParse(string.Join("", word2.Where(c => char.IsDigit(c))), out value2);
                    if (value1 > value2)
                    {
                        Swap(j, j + 1);
                    }
                }
            }

            return result;
        }
        public static string Order(string words)
        {
            if (string.IsNullOrEmpty(words)) return words;
            return string.Join(" ", words.Split(' ').OrderBy(s => s.ToList().Find(c => char.IsDigit(c))));
            //if (words.Length == 0)
            //{
            //    return "";
            //}
            //string[] words_ = words.Split(' ');
            //result = OrderBy(words_);
            //StringBuilder result_ = new StringBuilder();
            //for (int i = 0; i < result.Length; i++)
            //{
            //    result_.Append(result[i] + " ");
            //}
            //return result_.ToString().Trim();
        }
    }
}
