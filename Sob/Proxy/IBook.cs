﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Proxy
{
    interface IBook : IDisposable
    {
        Page GetPage(int number);
    }
}
