﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Proxy
{
    class PageContext : DbContext
    {
        public DbSet<Page> Pages { get; set; }
    }
}
