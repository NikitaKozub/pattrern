﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Task.Morze._4
{
    public class MorseCodeDecoder
    {
        /// <summary>
        /// Очистка от лишних нулей в начале и в конце
        /// </summary>
        private static void ClearZeroBeginLine()
        {
            for (int i = 0; i < strBits.Length; i++)
            {
                if (strBits[i] == '0')
                {
                    strBits.Remove(i, 1);
                    i--;
                    continue;
                }
                if (strBits[i] == '1')
                {
                    break;
                }
            }
            for (int i = strBits.Length-1; i > 0; i--)
            {
                if (strBits[i] == '0')
                {
                    strBits.Remove(i, 1);
                    continue;
                }
                if (strBits[i] == '1')
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Вычисление скорости по 1
        /// </summary>
        private static void CalculationSpeedOne()
        {
            for (int i = 0; i < strBits.Length; i++)
            {
                if (strBits[i] == '1')
                {
                    newSpeed++;
                }
                if (strBits[i] == '0')
                {
                    //break;
                    if (newSpeed != 0 && newSpeed < speed)
                    {
                        speed = newSpeed;
                        newSpeed = 0;
                    }
                    newSpeed = 0;
                }
            }
            //if (newSpeed != speed)
            //{
            //    speed = newSpeed;
            //    newSpeed = 0;
            //    return;
            //}
            newSpeed = 0;
        }

        private static int newSpeed = 0;
        /// <summary>
        /// Вычисление скорости по 0
        /// </summary>
        private static void CalculationSpeedZero()
        {
            for (int i = 0; i < strBits.Length; i++)
            {
                if (strBits[i] == '0')
                {
                    i++;
                    for (int j = i; j < strBits.Length; j++)
                    {
                        newSpeed++;
                        if (strBits[j] == '1')
                        {
                            if (newSpeed != 0 && newSpeed <= speed)
                            {
                                speed = newSpeed;
                                newSpeed = 0;
                                return;
                            }
                            newSpeed = 0;
                            return;
                        }
                    }
                }
            }            
        }

        private static int speed;//= 1;
        /// <summary>
        /// Вычисление скорости
        /// </summary>
        private static void CalculatorSpeed()
        {
            speed = 10000;
            CalculationSpeedOne();
            CalculationSpeedZero();
        }

        static StringBuilder strBits;
        static int countSymvolov1 = 0;
        static int countZero = 0;
        static int countSymvolovSpace;
        static int countSymvolovSpaceWord;
        //public static string DecodeBits(string bits)
        //{
        //    strBits = new StringBuilder(bits);
        //    ClearZeroBeginLine();
        //    CalculatorSpeed();
        //    countSymvolov1 = 0;
        //    countZero = 0;
        //    countSymvolovSpace = 3 * speed;
        //    countSymvolovSpaceWord = 7 * speed;
        //    for (int i = 0; i < strBits.Length; i++)
        //    {
        //        if (strBits[i] == '1')
        //        {
        //            countSymvolov1++;
        //            if (i + 1 == strBits.Length)
        //            {
        //                i++;
        //                strBits.Remove(i - countSymvolov1, countSymvolov1 - 1);
        //                if (countSymvolov1 == countSymvolovSpace)
        //                {
        //                    strBits[i - countSymvolov1] = '-';
        //                    continue;
        //                }
        //                if (countSymvolov1 <= speed)
        //                {
        //                    strBits[i - countSymvolov1] = '.';
        //                    continue;
        //                }
        //            }
        //            continue;
        //        }
        //        if (strBits[i] == '0' && countSymvolov1 == countSymvolovSpace)
        //        {
        //            strBits.Remove(i - countSymvolov1, countSymvolov1 - 1);
        //            strBits[i - countSymvolov1] = '-';
        //            i = i - (countSymvolov1 - 1);
        //            i = endSymvol(i, countSymvolovSpace, countSymvolovSpaceWord);
        //        }

        //        if (strBits[i] == '0' && countSymvolov1 <= speed)
        //        {
        //            strBits.Remove(i - speed, countSymvolov1 - 1);
        //            strBits[i - speed] = '.';
        //            i = i - (countSymvolov1 - 1);
        //            i = endSymvol(i, countSymvolovSpace, countSymvolovSpaceWord);
        //        }
        //    }
        //    return strBits.ToString();
        //}

        public static string DecodeBits(string bits)
        {
            var cleanedBits = bits.Trim('0');
            var rate = GetRate();
            return cleanedBits
              .Replace(GetDelimiter(7, "0"), "   ")
              .Replace(GetDelimiter(3, "0"), " ")
              .Replace(GetDelimiter(3, "1"), "-")
              .Replace(GetDelimiter(1, "1"), ".")
              .Replace(GetDelimiter(1, "0"), "");

            string GetDelimiter(int len, string c) => Enumerable.Range(0, len * rate).Aggregate("", (acc, _) => acc + c);
            int GetRate() => GetLengths("0").Union(GetLengths("1")).Min();
            IEnumerable<int> GetLengths(string del) => cleanedBits.Split(del, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Length);
        }

        public static int endSymvol(int i, int countSymvolovSpace, int countSymvolovSpaceWord)
        {
            for (int find_0 = i; find_0 < strBits.Length; find_0++)
            {
                if (strBits[find_0] == '0')
                {
                    countZero++;
                    continue;
                }

                if (strBits[find_0] == '1' && countZero == countSymvolovSpace)
                {
                    strBits.Remove(find_0 - countZero, countZero - 1);
                    strBits[find_0 - countZero] = ' ';
                    i = find_0 - (countZero - 1);
                    i--;
                    countSymvolov1 = 0;
                    countZero = 0;
                    break;
                }

                if (strBits[find_0] == '1' && countZero < countSymvolovSpace)
                {
                    strBits.Remove(find_0 - countZero, countZero);
                    i = find_0 - countZero;
                    i--;
                    countSymvolov1 = 0;
                    countZero = 0;
                    break;
                }

                if (strBits[find_0] == '1' && countZero == countSymvolovSpaceWord)
                {
                    strBits.Remove(find_0 - countZero, countZero);
                    strBits.Insert(find_0 - countZero, "   ");
                    i = find_0 - (countZero - 1);
                    i--;
                    countSymvolov1 = 0;
                    countZero = 0;
                    break;
                }
            }
            return i;
        }

        public static string DecodeMorse(string morseCode)
        {
            if (morseCode.Length == 0)
            {
                return morseCode;
            }
            string[] word = morseCode.Split("   ");
            var result = new string[word.Length][];
            for (int i = 0; i < word.Length; i++)
            {
                result[i] = word[i].Split(' ');
            }

            StringBuilder Result = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                for (int j = 0; j < result[i].Length; j++)
                {
                    //Result.Append(MorseCode.Get(result[i][j]));
                }
                if (i != result.Length - 1 && result[i][0] != "")
                    Result.Append(" ");
            }
            Console.WriteLine("Result " + Result);
            return Result.ToString();
        }
    }
}
