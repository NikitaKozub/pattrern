﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task._4
{
    public class Sudoku
    {
        int[][] SudokuSquares;
        int N;
        int[] Numbers;
        public Sudoku(int[][] sudokuData)
        {
            SudokuSquares = sudokuData;
        }

        public bool IsValid()
        {
            if (CheckedSize() == false ||
               CheckedVertical() == false ||
               CheckedGorizontal() == false ||
               CheckSquares() == false)
            {
                return false;
            }
            return true;
        }

        public bool CheckedVertical()
        {
            for (int j = 0; j < N; j++)
            {
                EnterNumberSudoku();
                for (int i = 0; i < SudokuSquares.Length; i++)
                {
                    for (int n = 0; n < N; n++)
                    {
                        if (SudokuSquares[i][j] == Numbers[n] && SudokuSquares[i][j] != -1)
                        {
                            Numbers[n] = -1;
                            break;
                        }
                        else
                        {
                            if (n + 1 == N)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        public bool CheckedGorizontal()
        {
            for (int i = 0; i < SudokuSquares.Length; i++)
            {
                EnterNumberSudoku();
                for (int j = 0; j < SudokuSquares[i].Length; j++)
                {
                    for (int n = 0; n < N; n++)
                    {
                        if (SudokuSquares[i][j] == Numbers[n] && SudokuSquares[i][j] != -1)
                        {
                            Numbers[n] = -1;
                            break;
                        }
                        else
                        {
                            if (n + 1 == N)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            
            return true;
        }

        public bool CheckedSize()
        {
            N = SudokuSquares.Length;
            for (int i = 0; i < SudokuSquares.Length; i++)
            {
                if (SudokuSquares.Length != SudokuSquares[i].Length)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Количество элементов
        /// </summary>
        public void EnterNumberSudoku()
        {
            Numbers = new int[SudokuSquares.Length];
            for (int i = 0; i < N; i++)
            {
                Numbers[i] = i + 1;
            }
        }

        public bool CheckSquares()
        {
            double koren = Math.Sqrt(N);
            int CountSquares = 0;
            if (koren % 1 != 0)
            {
                return false;
            }
            CountSquares = (int)koren;
            for (int i = 0; i < N; i = i + CountSquares)
            {
                for (int j = 0; j < N; j = j + CountSquares)
                {
                    if(MoveSquares(i, j, CountSquares) == false)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool MoveSquares(int startI, int startJ, int countSquares)
        {
            EnterNumberSudoku();
            for (int i = startI; i < startI + countSquares; i++)
            {
                for (int j = startJ; j < startJ + countSquares; j++)
                {
                    for (int n = 0; n < N; n++)
                    {
                        if (SudokuSquares[i][j] == Numbers[n] && SudokuSquares[i][j] != -1)
                        {
                            Numbers[n] = -1;
                            break;
                        }
                        else
                        {
                            if (n + 1 == N)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}
