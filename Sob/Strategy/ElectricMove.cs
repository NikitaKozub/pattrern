﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Strategy
{
    class ElectricMove : IMovable
    {
        public void Move()
        {
            Console.WriteLine("Перемещение на электричестве");
        }
    }
}
