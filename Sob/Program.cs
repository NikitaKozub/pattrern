﻿using Sob.Adapter;
using Sob.Bridge;
using Sob.Builder;
using Sob.ChainResponsibility;
using Sob.Composite;
using Sob.Decorator;
using Sob.Facade;
using Sob.Flyweight;
using Sob.Prototype;
using Sob.Proxy;
using Sob.Stack;
using Sob.Strategy;
using Sob.tree;
using Sob.VirtualObberideNew;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Task.CodeWars;

namespace Sob
{
    class Program
    {
        static void Main(string[] args)
        {
            // Паттерн --------------Builder---------------
            //// содаем объект пекаря
            //Baker baker = new Baker();
            //// создаем билдер для ржаного хлеба
            //BreadBuilder builder = new RyeBreadBuilder();
            //// выпекаем
            //Bread ryeBread = baker.Bake(builder);
            //Console.WriteLine(ryeBread.ToString());
            //// Cоздаем билдер для пшеничного хлеба
            //builder = new WheatBreadBuilder();
            //Bread wheatBread = baker.Bake(builder);
            //Console.WriteLine(wheatBread.ToString());

            //Console.Read();

            // Паттерн --------------Prototype-------------
            //IFigure figure = new Rectangle(30, 40);
            //IFigure clonedFigure = figure.Clone();
            //figure.GetInfo();
            //clonedFigure.GetInfo();

            //figure = new Circle(30);
            //clonedFigure = figure.Clone();
            //figure.GetInfo();
            //clonedFigure.GetInfo();

            //Console.Read();

            //// Паттерн--------------Adapter------------ -
            //// путешественник
            //Driver driver = new Driver();
            //// машина
            //Auto auto = new Auto();
            //// отправляемся в путешествие
            //driver.Travel(auto);
            //// встретились пески, надо использовать верблюда
            //Camel camel = new Camel();
            //// используем адаптер
            //ITransport camelTransport = new CamelToTransportAdapter(camel);
            //// продолжаем путь по пескам пустыни
            //driver.Travel(camelTransport);

            //Console.Read();

            //// Паттерн --------------Bridge-------------
            //// создаем нового программиста, он работает с с++
            //Programmer freelancer = new FreelanceProgrammer(new CPPLanguage());
            //freelancer.DoWork();
            //freelancer.EarnMoney();
            //// пришел новый заказ, но теперь нужен c#
            //freelancer.Language = new CSharpLanguage();
            //freelancer.DoWork();
            //freelancer.EarnMoney();

            //Console.Read();

            //// Паттерн --------------Composite-------------
            //Component fileSystem = new Directory("Файловая система");
            //// определяем новый диск
            //Component diskC = new Directory("Диск С");
            //// новые файлы
            //Component pngFile = new File("12345.png");
            //Component docxFile = new File("Document.docx");
            //// добавляем файлы на диск С
            //diskC.Add(pngFile);
            //diskC.Add(docxFile);
            //// добавляем диск С в файловую систему
            //fileSystem.Add(diskC);
            //// выводим все данные
            //fileSystem.Print();
            //Console.WriteLine();
            //// удаляем с диска С файл
            //diskC.Remove(pngFile);
            //// создаем новую папку
            //Component docsFolder = new Directory("Мои Документы");
            //// добавляем в нее файлы
            //Component txtFile = new File("readme.txt");
            //Component csFile = new File("Program.cs");
            //docsFolder.Add(txtFile);
            //docsFolder.Add(csFile);
            //diskC.Add(docsFolder);

            //fileSystem.Print();

            //Console.Read();

            //// Паттерн --------------Decorator-------------
            //Pizza pizza1 = new ItalianPizza();
            //pizza1 = new TomatoPizza(pizza1); // итальянская пицца с томатами
            //Console.WriteLine("Название: {0}", pizza1.Name);
            //Console.WriteLine("Цена: {0}", pizza1.GetCost());

            //Pizza pizza2 = new ItalianPizza();
            //pizza2 = new CheesePizza(pizza2);// итальянская пиццы с сыром
            //Console.WriteLine("Название: {0}", pizza2.Name);
            //Console.WriteLine("Цена: {0}", pizza2.GetCost());

            //Pizza pizza3 = new BulgerianPizza();
            //pizza3 = new TomatoPizza(pizza3);
            //pizza3 = new CheesePizza(pizza3);// болгарская пиццы с томатами и сыром
            //Console.WriteLine("Название: {0}", pizza3.Name);
            //Console.WriteLine("Цена: {0}", pizza3.GetCost());

            //Console.ReadLine();

            //// Паттерн --------------Facade-------------
            //TextEditor textEditor = new TextEditor();
            //Compiller compiller = new Compiller();
            //CLR clr = new CLR();

            //VisualStudioFacade ide = new VisualStudioFacade(textEditor, compiller, clr);

            //Sob.Facade.Programmer programmer = new Sob.Facade.Programmer();
            //programmer.CreateApplication(ide);

            //Console.Read();


            //// Паттерн --------------Flyweight-------------
            //double longitude = 37.61;
            //double latitude = 55.74;

            //HouseFactory houseFactory = new HouseFactory();
            //for (int i = 0; i < 5; i++)
            //{
            //    House panelHouse = houseFactory.GetHouse("Panel");
            //    if (panelHouse != null)
            //        panelHouse.Build(longitude, latitude);
            //    longitude += 0.1;
            //    latitude += 0.1;
            //}

            //for (int i = 0; i < 5; i++)
            //{
            //    House brickHouse = houseFactory.GetHouse("Brick");
            //    if (brickHouse != null)
            //        brickHouse.Build(longitude, latitude);
            //    longitude += 0.1;
            //    latitude += 0.1;
            //}

            //Console.Read();

            //// Паттерн --------------Proxy-------------
            //using (IBook book = new BookStoreProxy())
            //{
            //    // читаем первую страницу
            //    Page page1 = book.GetPage(1);
            //    Console.WriteLine(page1.Text);
            //    // читаем вторую страницу
            //    Page page2 = book.GetPage(2);
            //    Console.WriteLine(page2.Text);
            //    // возвращаемся на первую страницу    
            //    page1 = book.GetPage(1);
            //    Console.WriteLine(page1.Text);
            //}

            //Console.Read();

            //// Паттерн --------------Chain of responsibility-------------
            //Receiver receiver = new Receiver(false, true, true);

            //PaymentHandler bankPaymentHandler = new BankPaymentHandler();
            //PaymentHandler paypalPaymentHandler = new PayPalPaymentHandler();
            //PaymentHandler moneyPaymentHadler = new MoneyPaymentHandler();

            //bankPaymentHandler.Successor = paypalPaymentHandler;
            //paypalPaymentHandler.Successor = moneyPaymentHadler;

            //bankPaymentHandler.Handle(receiver);

            //Console.Read();

            //// Паттерн --------------Strategy-------------
            //Strategy.Car auto = new Strategy.Car(4, "Volvo", new PetrolMove());
            //auto.Move();
            //auto.Movable = new ElectricMove();
            //auto.Move();

            //Console.ReadLine();

            //// Паттерн --------------ChainResponsibility---
            //Receiver receiver = new Receiver(false, true, true);

            //PaymentHandler bankPaymentHandler = new BankPaymentHandler();
            //PaymentHandler moneyPaymentHadler = new MoneyPaymentHandler();
            //PaymentHandler paypalPaymentHandler = new PayPalPaymentHandler();
            //bankPaymentHandler.Successor = paypalPaymentHandler;
            //paypalPaymentHandler.Successor = moneyPaymentHadler;

            //bankPaymentHandler.Handle(receiver);

            //Console.Read();


            //A a2 = new B();
            //a2.aa();
            //a2.bb();

            //B b1 = new C();
            //b1.aa();
            //b1.bb();

            //C c = new C();
            //c.aa();
            //c.bb();


            //E e = new E(); 
            //E.IpsBetween("75.183.212.118", "76.25.134.129");

            //---------------------------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------------------------
            //---------------------------------------------------------------------------------------------------------
            // Если ли повторяющиеся символы в строке
            //string str = "assdfghjbb";
            //for (int i = 0; i < str.Length; i++)
            //{
            //    int idFirst = str.IndexOf(str[i], i);
            //    int idSecond = str.LastIndexOf(str[i]);
            //    if (idFirst != idSecond)
            //    {
            //        Console.WriteLine(str[i]);
            //    }
            //}
            //// Переверните строку
            //StringBuilder strReverse = new StringBuilder();
            //for (int i = str.Length - 1; i >= 0; i--)
            //{
            //    strReverse.Append(str[i]);
            //}
            //Console.WriteLine(strReverse);

            //// Стэк своими руками с сортировкой
            //var myStack = new MyStack<string>();
            //myStack.Push("a");
            //Console.WriteLine(myStack.Pop());
            //myStack.Push("s");
            //myStack.Push("d");
            //Console.WriteLine(myStack.Pop());
            //Console.WriteLine(myStack.Pop());
            //myStack.Push("v");
            //myStack.Push("r");
            //Console.WriteLine(myStack.Pop());
            //myStack.Push("g");
            //Console.WriteLine(myStack.Peek());
            //Console.WriteLine(myStack.Peek());
            //myStack.Push("n");
            //myStack.Push("m");
            //myStack.Push("n");
            //myStack.Push("t");
            //myStack.Push("b");
            //myStack.Push("p");
            //myStack.Push("o");
            //myStack.Push("q");
            //myStack.Sort();
            //foreach(var itr in myStack)
            //{
            //    Console.Write(itr + " ");
            //}

            // Бинарное дерево
            //var a_node = new BinaryTree<char>('a');
            //a_node.AddLefthNode('b');
            //a_node.AddRightNode('c');

            //var b_node = a_node.left;
            //b_node.AddRightNode('d');

            //var c_node = a_node.right;
            //c_node.AddLefthNode('e');
            //c_node.AddRightNode('f');

            //var d_node = b_node.right;
            //var e_node = c_node.left;
            //var f_node = c_node.right;

            //Console.WriteLine(a_node.value); //# a
            //Console.WriteLine(b_node.value); //# b
            //Console.WriteLine(c_node.value); //# c
            //Console.WriteLine(d_node.value); //# d
            //Console.WriteLine(e_node.value); //# e
            //Console.WriteLine(f_node.value); //# f
            //a_node.DFSPreOrder(a_node);
            //a_node.DFSInOrder(a_node);
            //a_node.DFSPostOrder(a_node);
            //a_node.BFS(a_node);
            //Console.WriteLine("---------------");
            //foreach (var itr in BinaryTree<char>.Deep)
            //{
            //    Console.Write(itr + " ");
            //}

            // Бинарное дерево поиска
            var tree = new BinaryTreeFind<int>(50);
            tree.AddNewNode(tree, 76);
            tree.AddNewNode(tree, 21);
            tree.AddNewNode(tree, 4);
            tree.AddNewNode(tree, 32);
            tree.AddNewNode(tree, 100);
            tree.AddNewNode(tree, 64);
            tree.AddNewNode(tree, 32);
            tree.AddNewNode(tree, 52);

            //tree.DFSPreOrder(tree);
            //foreach (var itr in BinaryTreeFind<int>.Deep)
            //{
            //    Console.Write(itr + " ");
            //}
            //if (tree.FindNode(tree, 32) != null)
            //{
            //    Console.WriteLine(tree.FindNode(tree, 32).value);
            //}
            //if (tree.FindNode(tree, 100) != null)
            //{
            //    Console.WriteLine(tree.FindNode(tree, 100).value);
            //}
            //if (tree.FindNode(tree, 33) != null)
            //{
            //    Console.WriteLine(tree.FindNode(tree, 33).value);
            //}
            //else
            //{
            //    Console.WriteLine("false");
            //}

            tree.DFSPreOrder(tree);
            foreach (var itr in BinaryTreeFind<int>.Deep)
            {
                Console.Write(itr + " ");
            }
            tree.DeleteNode(tree, tree, 21);
            Console.WriteLine("delete 21");
            BinaryTreeFind<int>.Deep.Clear();
            tree.DFSPreOrder(tree);
            foreach (var itr in BinaryTreeFind<int>.Deep)
            {
                Console.Write(itr + " ");
            }
        }


        public class Adam
        {
            private static Adam Adam_;
            private Adam()
            {
            }

            public static Adam GetInstance()
            {
                if (Adam_ == null)
                    Adam_ = new Adam();
                return Adam_;
            }
        }
    }
}
