﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Bridge
{
    interface ILanguage
    {
        void Build();
        void Execute();
    }
}
