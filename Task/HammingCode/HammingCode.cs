﻿using System;
using System.Text;

public class HammingCode
{
    static StringBuilder Result = new StringBuilder();

    public static string encode(string text)
    {
        ConvertStringToASCIIValueByte(text);
        ConvertEightByte();
        TripleEveryBit();
        string result = Result.ToString();
        Result.Clear();
        return result;
    }

    public static string decode(string bits)
    {
        AntyTripleEveryBit(bits);
        Convert8bitBinaryToASCIIValues();
        ConvertByteToASCIIValue(_8bitBinaryToASCIIValues);
        string result = Result.ToString();
        Result.Clear();
        return result;
    }

    static byte[] _8bitBinaryToASCIIValues;
    private static void Convert8bitBinaryToASCIIValues(){
        _8bitBinaryToASCIIValues = new byte[decodeResult.Length/8];
        for (int i = 0; i < decodeResult.Length; )
        {
            _8bitBinaryToASCIIValues[i/8] = Convert.ToByte(decodeResult.ToString(i, 8), 2);
            i = i + 8;
        }        
    }


    static StringBuilder decodeResult;
    private static void AntyTripleEveryBit(string bits)
    {
        decodeResult = new StringBuilder();
        bool step = false;
        char symbol;
        if (bits.Length == 0)
        {
            return;
        }
        for (int i = 0; i < bits.Length; )
        {
            symbol = bits[i];
            for (int j = i; j < i+3; j++)
            {
                if (symbol != bits[j])
                {
                    step = !step;
                    symbol = bits[j];
                }
            }
            i = i + 3;
            if (step == false)
            {
                decodeResult.Append(bits[i - 3]);
            }
            else
            {
                decodeResult.Append(bits[i - 2]);
            }
            step = false;
        }
    }

    private static void TripleEveryBit()
    {
        for (int i = 0; i < eightBytes.Length; i++)
        {
            for (int j = 0; j < eightBytes[i].ToString().Length; j++)
            {
                if (eightBytes[i][j] != ' ')
                {
                    Result.Append(eightBytes[i][j]);
                    Result.Append(eightBytes[i][j]);
                    Result.Append(eightBytes[i][j]);
                }
            }      
        }
    }

    private static Byte[] encodedBytes;
    private static void ConvertStringToASCIIValueByte(string text)
    {
        Encoding ascii = Encoding.ASCII;
        encodedBytes = ascii.GetBytes(text);
    }

    private static void ConvertByteToASCIIValue(Byte[] bytes)
    {
        Result.Append(System.Text.Encoding.ASCII.GetString(bytes));
    }

    private static string[] eightBytes;
    public static void ConvertEightByte()
    {
        if (encodedBytes.Length == 0)
        {
            return;
        }
        eightBytes = new string[encodedBytes.Length];
        StringBuilder tmp = new StringBuilder();
        for (int i = 0; i < encodedBytes.Length; i++)
        {
            tmp.Append(Convert.ToString(encodedBytes[i], 2));
            eightBytes[i] = tmp.ToString().PadLeft(8, '0') + ' ';
            tmp.Clear();
        }
    }
}