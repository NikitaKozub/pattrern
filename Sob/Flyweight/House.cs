﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Flyweight
{
    abstract class House
    {
        protected int stages; // количество этажей

        public abstract void Build(double longitude, double latitude);
    }
}
