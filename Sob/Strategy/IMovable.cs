﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Strategy
{
    interface IMovable
    {
        void Move();
    }
}
