﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob.Adapter
{
    interface ITransport
    {
        void Drive();
    }
}
