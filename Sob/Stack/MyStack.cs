﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Sob.Stack
{/*where T : IComparable<T>*/
    public class MyStack<T> : IEnumerable, IEnumerator, IComparer<T> where T : IComparable<T>
    {
        private List<T> stack;
        int index = -1;

        public MyStack()
        {
            stack = new List<T>();
        }

        /// <summary>
        /// Добавить элемент
        /// </summary>
        /// <param name="element">элемент</param>
        public void Push(T element)
        {
            stack.Add(element);
        }

        /// <summary>
        /// Получить последний добавленный элемент и удалить его из стэка
        /// </summary>
        /// <returns></returns>
        public T Pop()
        {
            if (stack.Count == 0)
            {
                throw new InvalidOperationException();
            }
            T element = stack[stack.Count - 1];
            stack.Remove(element);
            return element;
        }

        /// <summary>
        /// Получить последний добавленный элемент
        /// </summary>
        /// <returns></returns>
        public T Peek()
        {
            if (stack.Count == 0)
            {
                throw new InvalidOperationException();
            }
            return stack[stack.Count - 1];
        }

        public int Compare(T x, T y)
        {
            if (x.CompareTo(y) > 0)
            {
                return 1;
            }
            if (x.CompareTo(y) < 0)
            {
                return -1;
            }
            if (x.CompareTo(y) == 0)
            {
                return 0;
            }
            throw new NotImplementedException();
        }

        private void Swap(int IdElement1, int IdElement2)
        {
            T tmp = tmpSort[IdElement1];
            tmpSort[IdElement1] = tmpSort[IdElement2];
            tmpSort[IdElement2] = tmp;
        }

        T[] tmpSort;
        public void Sort()
        {
            tmpSort = new T[stack.Count];
            for (int i = 0; i < stack.Count; i++)
            {
                tmpSort[i] = stack[i];
            }

            for (int i = 0; i < tmpSort.Length; i++)
            {
                for (int j = 0; j < tmpSort.Length - 1 - i; j++)
                {
                    if (Compare(tmpSort[j], tmpSort[j + 1]) > 0)
                    {
                        Swap(j, j + 1);
                    }
                }
            }
            stack.Clear();
            for (int i = 0; i < tmpSort.Length; i++)
                stack.Add(tmpSort[i]);
        }

        public IEnumerator GetEnumerator()
        {
            return this;
        }

        public bool MoveNext()
        {
            if (index == stack.Count - 1)
            {
                Reset();
                return false;
            }

            index++;
            return true;
        }
        public void Reset()
        {
            index = -1;
        }

        public object Current
        {
            get
            {
                return stack[index];
            }
        }
    }
}
