﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task.CodeWars
{
    public class IterativeRotationCipher
    {
        static List<int> positionSpace;
        public static void Step1Encoding(StringBuilder str)
        {
            positionSpace = new List<int>();
            for (int i = 0; i < @str.Length; i++)
            {
                if (str[i] == ' ')
                {
                    positionSpace.Add(i + positionSpace.Count);
                    str.Remove(i, 1);
                    if (i != 0) i--;
                }
            }
        }

        private static StringBuilder tmpShift;
        public static StringBuilder Step2Encoding(string str, int n)
        {
            tmpShift = new StringBuilder();
            if (str.Length == 0)
            {
                tmpShift.Append(str);
            }
            int shift = n % str.Length;
            return tmpShift.Append(str.Substring(str.Length - shift) + str.Substring(0, str.Length - shift));
        }

        public static StringBuilder Step2Decoding(string str, int n)
        {
            tmpShift = new StringBuilder();
            if (str.Length == 0)
            {
                tmpShift.Append(str);
            }
            int shift = n % str.Length;
            return tmpShift.Append(str.Substring(shift) + str.Substring(0, shift));
        }

        public static void Step3Encoding(StringBuilder str)
        {
            foreach(var itr in positionSpace)
            {
                str.Insert(itr, ' ');
            }
        }
        private static StringBuilder tmp;
        public static void Step4Encoding(StringBuilder str, int n)
        {
            int start = 0;
            int itr;
            for(int i = 0; i < str.Length; i++)
            {
                if (str[i] == ' ' || i == str.Length - 1)
                {
                    if (i == str.Length - 1 && str[i] != ' ')
                    {
                        i++;
                    }
                    tmp = Step2Encoding(str.ToString(start, i - start), n);
                    itr = 0;
                    for (int j = start; j < i; j++)
                    {
                        str[j] = tmp[itr];
                        itr++;
                    }
                    start = i + 1;
                }
            }
        }

        public static void Step4Decoding(StringBuilder str, int n)
        {
            int start = 0;
            
            int itr;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == ' ' || i == str.Length - 1)
                {
                    if (i == str.Length - 1 && str[i] != ' ')
                    {
                        i++;
                    }
                    tmp = Step2Decoding(str.ToString(start, i - start), n);
                    itr = 0;
                    for (int j = start; j < i; j++)
                    {
                        str[j] = tmp[itr];
                        itr++;
                    }
                    start = i + 1;
                }
            }
        }

        public string Encode(int n, string s)
        {
            StringBuilder stringEncode = new StringBuilder(s);
            for (int i = 0; i < n; i++)
            {
                Step1Encoding(stringEncode);
                stringEncode = Step2Encoding(stringEncode.ToString(0, stringEncode.Length), n);
                Step3Encoding(stringEncode);
                Step4Encoding(stringEncode, n);
            }
            string result = n + " " + stringEncode.ToString();
            return result;
        }
        public string Decode(string s)
        {
            int n = 0;
            StringBuilder stringDecode = new StringBuilder(s);
            for (int i = 0; i < stringDecode.Length; i++)
            {
                if(stringDecode[i] == ' ')
                {
                    n = int.Parse(stringDecode.ToString().Substring(0, i));
                    stringDecode.Remove(0, i+1);
                    break;
                }
            }
            for (int i = 0; i < n; i++)
            {
                Step4Decoding(stringDecode, n);
                Step1Encoding(stringDecode);
                stringDecode = Step2Decoding(stringDecode.ToString(0, stringDecode.Length), n);
                Step3Encoding(stringDecode);
            }
            return stringDecode.ToString();
        }
    }
}
