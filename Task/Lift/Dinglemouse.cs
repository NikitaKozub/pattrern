﻿using System;
using System.Collections.Generic;

namespace Task.Lift
{
    public class Dinglemouse
    {
        static int CapacityOfPeople = 0;
        static int CapacityLift;
        static int[][] Queues;
        static List<int> Result;
        static List<int> PeopleLift;
        static int CurrentFloor = 0;

        public static void GetCapacityOfPeople()
        {
            for (int i = 0; i < Queues.Length; i++)
            {
                CapacityOfPeople += Queues[i].Length;
            }
        }

        public static int[] TheLift(int[][] queues, int capacity)
        {
            Queues = queues;
            GetCapacityOfPeople();
            Result = new List<int>();
            PeopleLift = new List<int>(capacity);
            CapacityLift = capacity;

            if (Result.Count == 0)
            {
                Result.Insert(0, 0);
            }
            while (CapacityOfPeople > 0)
            {
                MoveLift();
            }
            if (Result.Count > 0 && Result[Result.Count - 1] != 0)
            {
                Result.Add(0);
            }
            if (Result.Count > 0 && Result[0] != 0)
            {
                Result.Insert(0, 0);
            }
            
            return Result.ToArray();
        }

        static bool WayUp = true;
        static bool WayDown = false;
        public static void MoveLift()
        {
            if (WayUp == true && CapacityOfPeople > 0)
            {
                MoveUpLift();
            }
            if (WayDown == true && CapacityOfPeople > 0)
            {
                MoveDownLift();
            }
        }

        static bool isExit;
        public static void MoveUpLift()
        {
            while (CurrentFloor < Queues.Length - 1)
            {
                ExitLift();
                if (Queues[CurrentFloor].Length != 0)
                {
                    EnterLiftUp();
                }
                isExit = false;
                CurrentFloor++;
            }
            WayUp = false;
            WayDown = true;
        }

        public static void MoveDownLift()
        {
            while (CurrentFloor > 0)
            {
                ExitLift();
                if (Queues[CurrentFloor].Length != 0)
                {
                    EnterLiftDown();
                }
                isExit = false;
                CurrentFloor--;
            }
            WayUp = true;
            WayDown = false;
        }

        /// <summary>
        /// Вошли в лифт
        /// </summary>
        public static void EnterLiftDown()
        {
            for (int i = 0; i < Queues[CurrentFloor].Length; i++)
            {
                if(CurrentFloor > Queues[CurrentFloor][i] && Queues[CurrentFloor][i] != -1 && CapacityLift > 0)
                {
                    CapacityLift--;
                    PeopleLift.Add(Queues[CurrentFloor][i]);
                    Queues[CurrentFloor][i] = -1;
                    if (isExit == false)
                    {
                        if (Result.Count > 0 && Result[Result.Count - 1] != CurrentFloor)
                        {
                            Result.Add(CurrentFloor);
                        }
                        isExit = true;
                    }
                }
                if (CurrentFloor > Queues[CurrentFloor][i] && Queues[CurrentFloor][i] != -1 && CapacityLift == 0)
                {
                    if (Result.Count > 0 && Result[Result.Count - 1] != CurrentFloor)
                    {
                        Result.Add(CurrentFloor);
                    }
                }
            }
        }

        /// <summary>
        /// Вошли в лифт на верх
        /// </summary>
        public static void EnterLiftUp()
        {
           for (int i = 0; i < Queues[CurrentFloor].Length; i++)
            {
                if (CurrentFloor < Queues[CurrentFloor][i] && Queues[CurrentFloor][i] != -1 && CapacityLift > 0)
                {
                    CapacityLift--;
                    PeopleLift.Add(Queues[CurrentFloor][i]);
                    Queues[CurrentFloor][i] = -1;
                    if (isExit == false)
                    {
                        if (Result.Count > 0 && Result[Result.Count - 1] != CurrentFloor)
                        {
                            Result.Add(CurrentFloor);
                        }
                        isExit = true;
                    }
                }
                if (CurrentFloor < Queues[CurrentFloor][i] && Queues[CurrentFloor][i] != -1 && CapacityLift == 0)
                {
                    if (Result.Count > 0 && Result[Result.Count - 1] != CurrentFloor)
                    {
                        Result.Add(CurrentFloor);
                    }
                }
            }
        }

        public static void ExitLift()
        {
            for (int i = 0; i < PeopleLift.Count; i++)
            {
                if (PeopleLift[i] == CurrentFloor)
                {
                    PeopleLift.RemoveAt(i);
                    i--;
                    CapacityLift++;
                    CapacityOfPeople--;
                    if (isExit == false)
                    {
                        if (Result.Count > 0 && Result[Result.Count - 1] != CurrentFloor)
                        {
                            Result.Add(CurrentFloor);
                        }
                        isExit = true;
                    }
                }
            }
        }
    }
}