﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sob
{
    public abstract class Transport : IXZ
    {
        private int MinSpeed = 0;
        protected abstract int MaxSpeed { get; set; }

        private int speed;
        public int Speed
        {
            get
            {
                return speed;
            }
            set
            {
                if (value >= MinSpeed && value <= MaxSpeed)
                {
                    speed = value;
                }
            }
        }

        public Transport(int maxSpeed)
        {
            MaxSpeed = maxSpeed;
        }

        public void Run()
        {
            Console.WriteLine("Speed " + speed);
        }
    }
}
