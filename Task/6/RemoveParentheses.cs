﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task._6
{
    public class RemoveParentheses
    {
        public string Remove(string s)
        {
            StringBuilder str = new StringBuilder(s);
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '(')
                {
                    for (int j = i + 1; j < str.Length; j++)
                    {
                        if (str[j] == '(')
                        {
                            break;
                        }
                        if (str[j] == ')')
                        {
                            str.Remove(i, j - i + 1);
                            i = -1;
                            break;
                        }
                    }
                }
            }
            return str.ToString();
        }
    }
}
